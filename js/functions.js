'use strict';

// База данных
var json_file_list = ["users_data.json","person_data.json","company_data.json","departament_data.json","position_data.json"];

//  Загрузка данных из файла базы
function getData(url) {
  return new Promise(function(resolve, reject) {
    // Делаем XHR запрос
    var dataArr = new XMLHttpRequest();
    dataArr.open('GET', url);

    dataArr.onload = function() {
      if (dataArr.status == 200) {
        // Выборка данных из файла
        resolve(dataArr.response);
      } else {
        var error = new Error(this.statusText);
        error.code = dataArr.status;
        reject(error);
      }
    };
    // ошибки сети
    dataArr.onerror = function() {
      reject(new Error("Data load error - getData()"));
    };
    // Делаем запрос    
    dataArr.send();
  }); // end Promise
}


// загрузка данных из таблицы (файла json) в локальное хранилище
function loadIntoLS(url) {
   return new Promise(function(resolve, reject) {
  // загружаем данные из файла
    resolve(getData(url));
  }).then(function(loadedData){
    // парсим в объект
    let JSONData = jQuery.parseJSON(loadedData);
    return JSONData;
  }).then(function(jData){
    // загружаем в LocalStorage на хранение 
    // имя текущей таблицы
    let table_name = jData.table+'_id';
    // Запись пары имя таблицы/следующее значение
    localStorage.setItem(table_name,jData.next_id);
    // модель таблицы
    localStorage.setItem(table_name+'_model',JSON.stringify(jData.model));
    // console.log(jData.model);
    // Запись содержимого таблицы
    jQuery.each(jData.data,function( key, val ) {
      // структура пары/значение => имя-таблицы_ID_код-записи => users_id_1 - первая запись таблицы users
      let itemName = table_name+'_'+key; 
      // преобразуем объект в строку
      let itemData = JSON.stringify(val);
      // запись в локальое хранилище
      localStorage.setItem(itemName,itemData);
    });
  });
}

// загрузка всей базы данных 
function loadDBinLS(lists) {
var loadDB = new Promise(function(resolve, reject) {
    jQuery.each(lists,function( key, val ) {
      loadIntoLS(val);
      resolve(true);
    });    
  });
}

// загрузка данных одной таблицы в массив
function loadAllfieldsFromTable(tblName) {
  let allData = '{';
  let dlmtr = '';
  for (let i = 1; i <= 50; i++) {
    let cur_key = tblName+'_id_'+i;
    if (localStorage.getItem(cur_key)) {
      allData = allData+dlmtr+'"'+i+'":'+localStorage.getItem(cur_key);
      dlmtr = ',';
    };
  };
  return allData+'}';
}

//  отображение списка сотрудников на странице
function renderUserBtns(datas) {
  jQuery('#users_list').empty();
  let users = JSON.parse(datas);

  // let prm = new Promise(function(resolve, reject) {
    
  jQuery.each(users,function( key, val ) {
    // setTimeout(function () {
    let html_element = '<button type="button" class="btn btn-default users_item" data-user_id="'+val.user_id+'">'+val.nickname+'</button>';
    jQuery('#users_list').append(html_element);
    // }, 1000);
  });

  // resolve(true);
  // }); // promise

}


// Выборка одной записи из хранилища по ID и имени таблицы
function getItemFromLS(tblName,id,addTblName) {
  let item_Name   = tblName+'_id_'+id;
  let user_data   = localStorage.getItem(item_Name);
  var user_json   = JSON.parse(user_data);
  return user_json;    
}

// отображение записи на странице
function renderView(jsonData,tblName,id) {

  jQuery.each(jsonData,function( key, val ) {
    // console.log(key+', '+val);
    // есть элемент такой
    if (jQuery('#'+key)) {
      // проверка на логотип
      if (key == 'logo') {
        let html_img = "<img src='"+val+"' style='max-width:150px;max-height:50px;'>";
        jQuery('#'+key).empty();
        jQuery('#'+key).append(html_img);
        val = '';
      } // /проверка на логотип

      jQuery('#edit_'+tblName).attr("data-tbl",tblName);
      jQuery('#edit_'+tblName).attr("data-id",id);
      jQuery('#del_'+tblName).attr("data-tbl",tblName);
      jQuery('#del_'+tblName).attr("data-id",id);
      // if (tblName == 'users') {alert(id)};

      if ((val != null) && (val != '')) {jQuery('#'+key).text(val);}
      if (val == null) {jQuery('#'+key).text('Нет данных!');} 
    }
  }); // /jQuery.each
}


// Загрузка полной модели сотрудника
function loadModelTable(tblName) {}

// Загрузка полной модели сотрудника
function loadUserModel(user_id) {
  // Таблица user
  let user = getItemFromLS('users',user_id);
  // console.log(user_id);
  // console.log(user);
  let person_id = user.person_id;
  let person_data = getItemFromLS('person',person_id);
  let departament_id = user.departament_id;
  let departament_data = getItemFromLS('departament',departament_id);
  let position_id = user.position_id;
  let position_data = getItemFromLS('position',position_id);
  let company_id = departament_data.company_id;  
  let company_data = getItemFromLS('company',company_id);
  renderView(user,'users',user_id);
  renderView(person_data,'person',person_id);
  renderView(departament_data,'departament',departament_id);
  renderView(position_data,'position',position_id);
  renderView(company_data,'company',company_id);
  let userModel = user;
  userModel = jQuery.extend(userModel,person_data);
  userModel = jQuery.extend(userModel,departament_data);
  userModel = jQuery.extend(userModel,position_data);
  userModel = jQuery.extend(userModel,company_data);
  return userModel;
}

// создание input'ов по модели
function createInput(userModel,tblName,action) {
  let col = '';
  var value = '';
  
  jQuery.each(userModel,function( key, val ) {
    if (action == 'edit') {value = val;}

      // Выводим поля без id
    if((key.indexOf('_id')+1) == false) {
        col = col+'<div class="form-group"><div class="input-group"><input type="text" class="form-control" id="'+action+key+'" placeholder="'+val+'" value="'+value+'" name="'+tblName+'_id['+key+']"><span class="input-group-addon">'+key+'</span></div></div>';
    } else {
      // прячем поля с ID
      col = col+'<input type="hidden" id="'+action+key+'" placeholder="'+val+'" value="'+value+'" name="'+tblName+'_id">';
    }
  });
  return col;
}

// Создаём форму редактирования
function createForm(action,tblName,id) {
  var tableName = tblName;
  var modalHeader = '';
  var htmlFormModel = '';

  if (tblName == 'users') {
    let users = getItemFromLS('users',id, true);
    htmlFormModel = createInput(users,'users',action);    
    let person_data = getItemFromLS('person',id, true);
    htmlFormModel = htmlFormModel + createInput(person_data,'person',action);  
    modalHeader = ' нового сотрудника';
    
  } else {
    var userModel = getItemFromLS(tblName,id);
    htmlFormModel = createInput(userModel,tblName,action); 
  }
  if (action === 'add') {
    jQuery('#myModalLabel').text('Добавление '+modalHeader);
    jQuery('#saveBtn').text('Добавить');
    jQuery('#modalForm').attr("data-tbl",tableName);
    jQuery('#modalForm').attr("action",'add');
    let tblName = jQuery("#modalForm").attr("data-tbl");
  };
  if (action == 'edit') {
    jQuery('#modalForm').attr("data-tbl",tblName);
    jQuery('#modalForm').attr("action",'edit');
    jQuery('#modalForm').attr("data-id",id);
    jQuery('#myModalLabel').text('Редактирование');
    jQuery('#saveBtn').text('Сохранить');
  }

  let htmlForm = '';
  let col = '';
  htmlForm = '<div class="row"><div class="col-sm-12">'+htmlFormModel+'</div><!-- /col-sm-12 --></div><!-- /row -->';

  if (action == 'del') {
    jQuery('#modalForm').attr("data-tbl",tblName);
    jQuery('#modalForm').attr("action",'del');
    jQuery('#modalForm').attr("data-id",id);
    jQuery('#myModalLabel').text('Удаление');
    jQuery('#saveBtn').text('Удалить');
    htmlForm = 'Запись будет удалена!';
  }

  return htmlForm;
}

function adding(allInputs){
    // if (tblName == 'users') {
    // получаем ID следующей записи
    var user_id = localStorage.getItem('users_id');
    var id = user_id;
    
    // увеличиваем счётчик
    let user_id_next = parseInt(user_id)+1;
    localStorage.removeItem('users_id');
    localStorage.setItem('users_id',user_id_next);
    // получаем ID следующей записи
    var person_id = localStorage.getItem('person_id');
    // увеличиваем счётчик
    let person_id_next = parseInt(person_id)+1;
    localStorage.removeItem('person_id');
    localStorage.setItem('person_id',person_id_next);

    // console.log('----------------------------------');
    // console.log('------------- MODEL --------------');
    // формируем запись для хранилища
    // USERS
    var user_id_model = localStorage.getItem('users_id_model');
    var person_id_model = localStorage.getItem('person_id_model');
    var model_user_str = '{"users_id":"'+user_id+'","person_id":"'+person_id+'",';
    var model_person_str = '{"person_id":"'+person_id+'",';
    var dot = '';
    var dots = '';

    jQuery.each(allInputs,function( key, val ) {

      if ((val.name.indexOf('first_name') >= 0) || (val.name.indexOf('nickname') >= 0)) {
        if (val.value == '') {
          id = false; 
          // return false;
        };
      };
      let name = val.name;
      let iOf = name.indexOf('[')+1;
      let iEnd = name.indexOf(']');
      let fieldName = name.substring(iOf,iEnd);
      let fieldValue = val.value;
      if (name.indexOf('users_id') >= 0) {
        model_user_str = model_user_str+dot+'"'+fieldName+'":"'+fieldValue+'"';
        dot = ',';
      };
      if (name.indexOf('person_id') >= 0) {        
        model_person_str = model_person_str+dots+'"'+fieldName+'":"'+fieldValue+'"';
        dots = ',';
      }

    });
    model_user_str = model_user_str+'}';
    model_person_str = model_person_str+'}';
    // console.log('====================================');
    
    var new_user = user_id_model;
    new_user = JSON.parse(new_user);
    var model_user = JSON.parse(model_user_str);
    var new_user_item = jQuery.extend(true,{},new_user,model_user);
    new_user_item.user_id = id;
    new_user_item = JSON.stringify(new_user_item);
    localStorage.setItem('users_id_'+id,new_user_item);

    var new_person = person_id_model;
    new_person = JSON.parse(new_person);
    var model_person = JSON.parse(model_person_str);
    var new_person_item = jQuery.extend(true,{},new_person,model_person);
    new_person_item = JSON.stringify(new_person_item);
    localStorage.setItem('person_id_'+person_id,new_person_item);
  // }
  if (id === false) {
    return false;
  } else {
    localStorage.setItem('person_id_'+person_id,new_person_item);
    localStorage.setItem('users_id_'+id,new_user_item);    
    return id;
  }
}

function edit(allInputs,tblName,id) {
    // формируем запись для хранилища
    if (tblName == 'users') {
      let user_ls = localStorage.getItem('users_id_'+id);
      let user_json = JSON.parse(user_ls);
      let person_id = user_json.person_id;
      let person_ls = localStorage.getItem('person_id_'+person_id);
    // USERS
    var model_user_str = '{"users_id":"'+id+'",';
    var model_person_str = '{"person_id":"'+person_id+'",';
    var dot = '';
    var dots = '';
    jQuery.each(allInputs,function( key, val ) {
      let name = val.name;
      let iOf = name.indexOf('[')+1;
      let iEnd = name.indexOf(']');
      let fieldName = name.substring(iOf,iEnd);
      let fieldValue = val.value;
      if (name.indexOf('users_id') >= 0) {
        model_user_str = model_user_str+dot+'"'+fieldName+'":"'+fieldValue+'"';
        dot = ',';
      };
      if (name.indexOf('person_id') >= 0) {        
        model_person_str = model_person_str+dots+'"'+fieldName+'":"'+fieldValue+'"';
        dots = ',';
      }

    });
    model_user_str = model_user_str+'}';
    model_person_str = model_person_str+'}';

    var new_user = user_json;
    var model_user = JSON.parse(model_user_str);
    var new_user_item = jQuery.extend(true,{},new_user,model_user);
    new_user_item = JSON.stringify(new_user_item);

    localStorage.removeItem('users_id_'+id);
    localStorage.setItem('users_id_'+id,new_user_item);

    localStorage.removeItem('person_id_'+person_id);
    localStorage.setItem('person_id_'+person_id,model_person_str);
  }; // USER
}

function del(allInputs,id) {  
    let tblName = jQuery("#modalForm").attr("data-tbl");
    var id = jQuery("#modalForm").attr("data-id");
    localStorage.removeItem(tblName+'_id_'+id);
    id = 1;
    var adg = localStorage.setItem('users_id_'+id,1);
}

// ----------------------------------------
// ----------------------------------------
// Исполнение функций при загрузке страницы
$(document).ready(function(){

// Выбор текущего сотрудника (click по кнопке)
// навешиваем событие на динамически созданную кнопку
jQuery("#users_list").on("click", ".btn.btn-default.users_item", function () {
    let user_id = jQuery(this).attr("data-user_id");
    loadUserModel(user_id);
});

// навешиваем событие на добавление сотрудника
jQuery('.btn.add').bind('click', function(){
  let tbl = jQuery(this).attr("data-tbl");
  jQuery('#modal_form_user').empty();
  jQuery('#modal_form_user').append(createForm('add',tbl,1));
});
// навешиваем событие на редактирование сотрудника
jQuery('.btn.edit').bind('click', function(){
  let tbl = jQuery(this).attr("data-tbl");
  let id = jQuery(this).attr("data-id");
  jQuery('#modal_form_user').empty();
  jQuery('#modal_form_user').append(createForm('edit',tbl,id));
});
// навешиваем событие на удаление сотрудника
jQuery('.btn.del').bind('click', function(){
  let tbl = jQuery(this).attr("data-tbl");
  let id = jQuery(this).attr("data-id");
  // console.log(tbl+'    '+id);
  jQuery('#modal_form_user').empty();
  jQuery('#modal_form_user').append(createForm('del',tbl,id));
});


// -----------   Press button form  -----------
jQuery('#saveBtn').click(function(){
  let action = jQuery("#modalForm").attr("action");
  let tblName = jQuery("#modalForm").attr("data-tbl");
  var id = jQuery("#modalForm").attr("data-id");
  let allInputs =  jQuery("input:text");
  // -------------------------------
  // ---------- ADD  -----------
  if (action == 'add') {
    id = adding(allInputs);
    if (!id) {
      var hdr = 'Уведомление: Добавление сотрудника';
      var msg = 'Не заполнены поля NICKNAME и FIRST_NAME!';
      var status = 'danger';
    } else {      
      var hdr = 'Уведомление: Добавление сотрудника';
      var msg = 'Новый сотрудник добавлен!';
      var status = 'success';
    }
    $('#jGrowl-container1').jGrowl({
          header: hdr,
          message: msg,
          group: 'alert-' + status,
          life: 5000
        });
  }; // Adding new user
  // -------------------------------
  // ---------- EDIT  -----------
  if (action == 'edit') {
    edit(allInputs,tblName,id);
    $('#jGrowl-container1').jGrowl({
          header: 'Уведомление: Редактирование сотрудника',
          message: 'Данные о сотруднике изменены!',
          group: 'alert-' + 'success',
          life: 5000
        });
  }; // Editing user

  // -------------------------------
  // ---------- DELETING  -----------
  if (action == 'del') {
    del(allInputs,id);
    $('#jGrowl-container1').jGrowl({
          header: 'Уведомление: Удаление сотрудника',
          message: 'Сотрудник удалён!',
          group: 'alert-' + 'warning',
          life: 5000
        });
  }

  // отображение сотрудников на странице
    return new Promise(function(resolve, reject) {
      setTimeout(() => {    
        let allUsersList = loadAllfieldsFromTable(tblName);
        renderUserBtns(allUsersList);

        // выводим на страницу данные о сотруднике
        renderView(getItemFromLS(tblName,id),tblName,id);
        loadUserModel(id);
        resolve(true);
        }, 1500);
    });
});
// ----------- / Press button form -----------
// ----------------------------------------


// загрузка базы в локальное хранилище
loadDBinLS(json_file_list);

// отображение сотрудников на странице
let allUsersList = loadAllfieldsFromTable('users');
renderUserBtns(allUsersList);

// выводим на страницу данные о сотруднике по умолчанию
renderView(getItemFromLS('users',1),'users',1);
// console.log(getItemFromLS('users',1));
loadUserModel(1);

// console.log(localStorage);
localStorage.clear();
}); // end $(document).ready
// ----------------------------------------
// ----------------------------------------